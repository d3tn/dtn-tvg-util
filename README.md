# dtn-tvg-util

This project enables the creation and processing of Delay- and Disruption-tolerant Networking (DTN) scenarios in the form of contact plans and time-varying network graphs.
This functionality can be taken as a basis for DTN simulation tools.
`dtn-tvg-util` supports Python 2.7 and Python 3.5+.

## Requirements

* Python 2.7 or 3.5+
* a build toolchain for your platform plus the `python-dev` package to install the Python dependencies
* to place Ring Road ground stations, `libgeos-dev` and `libgdal-dev`

## Getting Started

For just using this module, install it via `pip`:

```
pip install dtn-tvg-util
```

For the development setup, you should first create a virtual environment and install with `pip install -e`:

```
python3 -m venv --without-pip .venv
curl -sS https://bootstrap.pypa.io/get-pip.py | .venv/bin/python
source .venv/bin/activate
pip install -e .
```

Some example tools can be found in `tvgutil.tools`. These scripts allow to generate and convert time-varying graph representations of Ring Road [1] scenarios.

For using the Ring Road scenario generators, you have to install some further dependencies:

```
pip install "dtn-tvg-util[ring_road]"
# OR
pip install -e ".[ring_road]"
```

For ground station placement (via `tvgutil/tools/create_gs_list.py`), also install the `gs_placement` extras by adding `[ring_road,gs_placement]` to the `pip install` command.

## Usage Examples

You can find an example of the scripts being applied in the [`aiodtnsim` repository](https://gitlab.com/d3tn/aiodtnsim/-/blob/master/examples/example_test_run.sh). The [`aiodtnsim` simulation script](https://gitlab.com/d3tn/aiodtnsim/-/blob/master/simulation.py) leverages `dtn-tvg-util` modules for providing contact plans to the simulator and its routing algorithms.

## Testing

Unit tests for the core functionality are provided in `test/`. They are based on the Python `unittest` package. Use a test runner of your choice to execute them, e.g. `pytest` or `nose`.

```
pip install nose
python -m nose test
```

## Concepts

### Contact Types

`dtn-tvg-util` differentiates within its `tvgutil.contact_plan` module between two types of contact plans and their associated time-varying graph (TVG) representations:

- **Factual Contact Plan (FCP)**: A list of `Contact` instances, representing
  the actually-happening contacts (as they are observed by the nodes).
  This representation could be obtained, for example, by observing a real
  network topology and recording the concrete links.
  An FCP is expected to be used, e.g., by an event-based simulator to represent
  the actual contacts that are encountered by the nodes over the course of the
  simulation.
- **Predicted Contact Plan (PCP)**: A list of `PredictedContact` instances,
  representing the contacts predicted by a node.
  Every node may have an individual PCP. This representation is supplied
  to routing algorithms such as Contact Graph Routing (CGR).

**Note:** In earlier versions, there was only one type of contact plan as the module was not aimed at testing different DTN routing algorithms, but only theoretical analyses of the network graph had to be performed. However, this has changed with the addition of more and more simulation functionalities.

In fully-deterministic scenarios, i.e., if all nodes always use an accurate
contact plan, a PCP can be converted into an FCP and
vice versa. In that case, the predicted characteristics are equal to the
factual characteristics and there is only one generation of predicted
characteristics.

### TVG Representation

The `tvgutil.tvg` module adds another representation of a network scenario in the form of a "time-varying network graph". Factual as well as predicted contact plans can be converted into a TVG representation. It is the preferred serialization format for scenarios generated by this package.

Overall, both a P-TVG and an F-TVG instances contain three properties:

- `vertices`: A `set` of nodes part of the graph.
- `edges`: A `dict` mapping tuples of (transmissing node, receiving node) to a list of contacts.
- `contact_type` The type (i.e., `Contact` or `PredictedContact`) of the contact instances contained in the values within the `edges` dictionary. his specified whether the `TVG` is a P-TVG or an F-TVG.

### Transmission Plans

Most existing DTN simulation toolchains generate messages (or _bundles_) "on the fly", while the simulation is being run. If randomness is involved, which is typically anticipated to add some variations to the simulated scenario, the simulation result becomes non-deterministic and is not easily reproducible (if not considering "workarounds" such as fixed random seeds).

`dtn-tvg-util` promotes another approach: A "transmission plan" can be generated by the `tvgutil.transmission_plan` module. This list of messages to be generated is later provided as input to a simulation tool, which creates the messages at the times specified in the plan. This way, every simulation run is fully deterministic and can be easily reproduced given the same input data. If variation is needed, several transmission plans can be generated for a single scenario.

## License

This code is licensed under the MIT license. See [LICENSE](LICENSE) for details.

## References

[1] Ring Road Networks (RRN) are Disruption-tolerant Networks (DTN) for communication, based on the deployment of low-cost LEO satellites. For more information, see the following paper: *S. C. Burleigh and E. J. Birrane, “Toward a Communications Satellite Network for Humanitarian Relief,” in International Conference on Wireless Technologies for Humanitarian Relief, Amritapuri, India, 2011.*
